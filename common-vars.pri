#-----------------------------------------------------------------------------
# Common variables for all projects.
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# Project name (used e.g. in include file and doc install path).
# remember to update debian/* files if you changes this
#-----------------------------------------------------------------------------
PROJECT_NAME = lomiri-system-settings-online-accounts

#-----------------------------------------------------------------------------
# Project version
# remember to update debian/* files if you changes this
#-----------------------------------------------------------------------------
PROJECT_VERSION = 0.12

# End of File
